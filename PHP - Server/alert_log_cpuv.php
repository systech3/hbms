<?php 
//session_start();
include("helper/DBOperation.php");
$dbObj = new DBOperation();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />

<title>HBMS - Alert Log</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>


</head>

<body>

<div id="alert_log_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        <?php
	$device_id = $_REQUEST['did'];
	$result_devicename = array();
	if($device_id) {
		$result_devicename = $dbObj->executeQuery("SELECT device_name FROM tbl_device where id=".$device_id , true);
	
	}
?>
        <div class="component_cpu_middle_content">
        
        	<div class="system_unit_row">
                <div class="system_top_home_bttn"><a href="component_CPU.php?cid=<?php echo $_REQUEST['cid']; ?>&did=<?php echo $_REQUEST['did']; ?>">Back</a></div>
                <div class="component_cpu_txt">
                	<div class="component_cpu_txt1"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?></div>
                    <div class="component_cpu_txt2">CPUV</div>
                </div>
            </div>
            
            <div class="alert_log_middle_content">
		<div class="alert_log_middle_top_row"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?> 
			CPUV Alert log
                </div>
		
		<?php 
			$component_id = $_REQUEST['cid'];
			$alertLogResult = $dbObj->getRecords("tbl_component_cpu", "", array('component_id'=>$component_id), "", "");
	
			$row_component = mysql_fetch_array($alertLogResult);
			//$alertLogResult = $dbObj->getRecords("tbl_log_message", "", array('component_id'=>$component_id), "", "");

$alertLogResult = mysql_query("SELECT * FROM tbl_log_message WHERE component_id=".$component_id." ORDER BY id DESC");
			
			while($row_alertlog = mysql_fetch_array($alertLogResult)) { 
			if($row_alertlog['status'] ==  "1") {			
			$txt_clr = "green";
			}
			else if ($row_alertlog['status'] ==  "2") {			
			$txt_clr = "yellow";
			}
			else if ($row_alertlog['status'] ==  "3") {			
			$txt_clr = "red";
			}
			else {
			$txt_clr = "white";
			}
			//echo "<BR><pre>";print_r($row_alertlog);
		?>
			
			<div class="alert_log_middle_content_row space">
				<div class="alert_middle_left_txt">
					<?php echo $row_alertlog['created_date'];?>
				</div>
				<div class="alert_middle_middle_txt"><?php echo $row_component['cpu_name'];?></div>
				<div class="alert_middle_right_txt" style="color:<?php echo $txt_clr; ?>"><?php echo $row_alertlog['message'];?></div>
			</div>
		<?php
			}
		?>
        </div></div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
			<div class="unita_bottm_home"><a href="unit_a.php"></a></div>
			<div class="unita_bottm_show_email"><a href="email_log.php"></a></div>
			<div class="unita_bottm_refresh"><a href="#" onclick="javascript:window.location.reload();"></a></div>
			<div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
			<div class="unita_bottm_setting"><a href="system.php?id=1"></a></div>
            	</div>
        </div>
        
        
    </div>
</div>



</body>
</html>
