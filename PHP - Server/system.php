<?php 

include("helper/DBOperation.php");
$dbObj = new DBOperation();

?>

<!DOCTYPE php PUBLIC "-//W3C//DTD Xphp 1.0 Transitional//EN" "http://www.w3.org/TR/xphp1/DTD/xphp1-transitional.dtd">

<php xmlns="http://www.w3.org/1999/xphp">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />

<title>HBMS - System</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top php element. Warning: BODY element is not compulsory so better to stop on php
while(noddy.nodeName !== "A" && noddy.nodeName !== "php") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>


</head>

<body>

<div id="system_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        <?php 
	$device_id = $_REQUEST['id'];
	$result_devicename = array();
	if($device_id) {
		$result_devicename = $dbObj->executeQuery("SELECT device_name FROM tbl_device where id=".$device_id , true);
	}

	?>
        <div class="system_unit_row">
                <div class="system_top_home_bttn"><a href="unit_a.php">Home</a></div>
                <div class="system_unit_txt"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?></div>
               <!-- <div class="system_unit_icon"><a href="alert_log.php"></a></div> -->
            </div>
        
        
        <div class="unita_middle_content_1">
        <?php
	    if($device_id) {
	    $componentResult = $dbObj->getRecords("tbl_components", "", array("device_id"=>$device_id), "", "");
	    $color_key_msg = array();
	    $color_key_msg['green'] = "online";
	    $color_key_msg['yellow'] = "issue";
	    $color_key_msg['red'] = "attention";
	    $color_key_msg['blue'] = "NA";

	    while($row_component = mysql_fetch_array($componentResult)) { 
		if($row_component['component_name']=='CPUH'){
		$link = "component_CPU.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='CPUV'){
		$link = "component_CPUV.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='Displays'){
		$link = "component_display.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='HVAC'){
		$link = "component_HVAC.php?cid=".$row_component['id']."&did=".$device_id;
		}

//////////Color Code Query
		
		$qry = "select * from tbl_component_state where component_id='".$row_component['id']."' order by datetime DESC limit 27";
		//echo "<pre>";print_r($qry);
		$result = mysql_query($qry);
		$total = mysql_num_rows($result);	
		$x = 0;
		$color_state = array();
		while($row=mysql_fetch_array($result))
		{
		    if($x%3 == 0) {
			    $color_state[] = $row['state'];
		    }
		    $x++;
		}
		

	?>
	    	<div class="unita_middle_row" style="width:auto;">
            		<div class="unita_progress_bar">
		<?php
		for($t=0; $t<9; $t++) { 
			
			if(isset($color_state[$t])) {
				$strip_color = $color_state[$t];	
			} else {
				$strip_color = "blue";
			}
			if($t==0) {
				$className = "big_strips";
				$msg = $color_key_msg[$strip_color];
			} else {
				$className = "small_strips";
				$msg = "";
			}
			?>
			
			<div id="s" class="<?php echo $className; ?>" style="background-color:<?php echo $strip_color; ?>"><?php echo $msg; ?></div>
			
		<?php } ?>
		</div>


                	<div class="unita_middle_txt" style="width:90px;"><?php echo $row_component['component_name']; ?></div>
                	<div class="unita_middle_arrow"><a href="<?=$link?>"></a> </div>
            	</div>
        	
	<?php } } ?>
        </div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
            	<div class="unita_bottm_home"><a href="unit_a.php"></a></div>
                <div class="unita_bottm_show_email"><a href="email_log.php"></a></div>
                <div class="unita_bottm_refresh"><a href="#"></a></div>
                <div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
                <div class="unita_bottm_setting"><a href="system.php?id=1"></a></div>
            </div>
        </div>
        
        
    </div>
</div>


</body>
</php>
