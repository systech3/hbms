<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />


<title>HBMS - Login</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>

<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	$("#login").click(function() {
	
		
	  document.getElementById("loading").style.display = "";
		//var action = $("#form1").attr('action');
		var form_data = {
			username: $("#username").val(),
			password: $("#password").val(),
			is_ajax: 1
		};		
		
		$.ajax({
			type: "POST",
			url: "doLogin.php",
			data: form_data,
			success: function(response)
			{				
				//alert(response);exit;
				if(response == 1 ){
					window.location.href = "unit_a.php";
				}else{
					document.getElementById("loading").style.display = "none";
					$("#message").html("<p class='error'>Invalid username and/or password.</p>");	
				}
			}
		});
		
		return false;
	});
	
});
</script>
</head>

<body>
	<form id="form1" name="form1" method="post">
    	<div class="login_content">
        	<div class="middle_screen">
            	<div class="cemusa_logo"></div>            	
                <div class="logo_textline">Digital Display Network Monitor</div>
                <div id="message"></div>
		 <!-- Show loading image if any operation is performed -->
		<div id="loading" style="display:none;">
 		<img id="loading-image" src="images/loader.gif" alt="Loading..." />
		</div>
<!-- End here image loading -->
                <div class="login_box">
                	<div class="user_name_row">
                    	<input type="text" id="username" title="User Name" value="username" onfocus="if ( this.value == this.defaultValue || this.value == '' ) { this.value = '';}" onblur="if ( this.value == '' ) { this.value = this.defaultValue;}" />
                    </div>
                    <div class="password_name_row">
                   		 <input type="password" id="password" title="Password" value="password" onfocus="if ( this.value == this.defaultValue || this.value == '' ) { this.value = '';}" onblur="if ( this.value == '' ) { this.value = this.defaultValue;}" />
                    </div>
                </div>
                <!--<div class="login_bttn">-->
                	<!--<a href="unit_a.html"></a>-->
                	
		<input type="submit" id="login" name="login" value=""  style="background:url(images/login_bttn.jpg)no-repeat;height: 30px;margin:20px 0 0 80px; border:none;
    width: 105px;
"  />
                </div>
		<div class="showtell_logo"></div><div id="message"></div>
            </div>
            
        </div>
        
       </form> 
        

        


</body>

</html>
