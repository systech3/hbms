
<?php 
//session_start(); 
include("helper/DBOperation.php");
$dbObj = new DBOperation();
$datatype = $_REQUEST['datatype'];
$insert_keyvalue = array();
$message = array();
$color_status = array();
$stripColor	= array();
$stripColor[]	= "green";
$color = "green";

//0 - white
//1 - green
//2 - yellow
//3 - red
//4 - blue

if($datatype == 1) {

	$cpu_temp = str_replace("oF","", $_REQUEST['cpu_tmp']);
	
	if(is_numeric($cpu_temp) && (($cpu_temp < 45) || ($cpu_temp > 75))) {
		$color_status[] = 2;
		$message[] = "temperature ".$cpu_temp ."<sup>o</sup>F";
		$color = "yellow";
		$stripColor[]	= "yellow";
	}
	
	$current_status = $_REQUEST['crnt_sts'];
	if($current_status== "offline" || $current_status== "") {
		$color_status[] = 3;
		$message[] = "shut down";
		$color = "red";
		$stripColor[]	= "red";
	}

	
	$cpu_temp = str_replace("oF","", $_REQUEST['cpu_tmp']);

	$network_speed = $_REQUEST['ntw_spd'];

	if(is_numeric($network_speed)) {
		if($network_speed < 8 && $network_speed >= 1) {
			$color_status[] = 2;
			$message[] = "Network Speed ".$network_speed." Mbps";
			$color = "yellow";
			$stripColor[]	= "yellow";
		}elseif($network_speed < 1 ) {
			$color_status[] = 3;
			$message[] = "Network Speed ".$network_speed." Mbps";
			$color = "red";
			$stripColor[]	= "red";
		}
	}

	$hd_space = $_REQUEST['hd_spc'];
	$free_hd_space = str_replace("Gb","", $hd_space);

	if(is_numeric($free_hd_space)) {
		if($free_hd_space < 40 &&  $free_hd_space >= 10) {
			$color_status[] = 2;
			$message[] = "Free HD space ".$free_hd_space." Gb";
			$color = "yellow";
			$stripColor[]	= "yellow";
		}elseif($free_hd_space < 10 ) {
			$color_status[] = 3;
			$message[] = "Free HD space ".$free_hd_space." Gb";
			$color = "red";
			$stripColor[]	= "red";
		}
	}
	// This is for finding out last ping.

 	  $sql = $dbObj->executeQuery("SELECT last_ping FROM tbl_component_cpu where component_id='".$_REQUEST['c_id']."' ORDER BY id DESC LIMIT 1", true);
	//echo "<pre>";print_r($sql);
  	//echo  gmdate($sql['last_ping'])."<br>".gmdate("Y-m-d h:i:s");
	if($sql['last_ping']){
	$currentPingTime 	= strtotime(gmdate("Y-m-d H:i:s"));
	$lastPingTime		= strtotime($sql['last_ping']) ;
	$pingDifference		= $currentPingTime - $lastPingTime ;
	$pingDifference		= round($pingDifference/60);
	}else{
		$pingDifference = 0;
	}
	//minutes to hour minute	
	$hours          = floor($pingDifference / 60);
	$decimalMinutes = $pingDifference - floor($pingDifference/60) * 60;

	# Put it together.
	$hoursMinutes = sprintf("%dH%02.0fM", $hours, $decimalMinutes);
//
	if($pingDifference >=5 ) {
		if($pingDifference <= 10){
			$color_status[] = 2;
			$message[] =  "last ping was : ".$hoursMinutes." ago";
			$color = "yellow";
			$stripColor[]	= "yellow";
		}
		else{
			$color_status[] = 3;
			$message[] ="last ping was : ".$hoursMinutes." ago";
			$color = "red";
			$stripColor[]	= "red";
		}		
	}
		
	$red	= "0";
	$yellow	= "0";
	for($i=0;$i<count($stripColor);$i++){
		if($stripColor[$i]=="red"){
			$red	= "1";
		}		
	}
	if($red=="0"){
		for($i=0;$i<count($stripColor);$i++){
			if($stripColor[$i]=="yellow"){
				$yellow	= "1";
			}		
		}
	}

	if($red=="1"){
		$color	= "red";		
	}
	else if($yellow=="1"){
		$color	= "yellow";
	}
	else{
		$color	= "green";
	}
	
	//echo $color;exit;
	
	
	
	$insert_keyvalue['id'] = NULL;
	$insert_keyvalue['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue['component_type'] = $_REQUEST['c_tpe'];
	$insert_keyvalue['cpu_name'] = $_REQUEST['cpu_nme'];
	$insert_keyvalue['ip_address'] = $_REQUEST['ip_ad'];
	$insert_keyvalue['last_ping'] = gmdate("Y-m-d H:i:s");
	$insert_keyvalue['current_status'] = $current_status;
	$insert_keyvalue['cpu_temp'] = $cpu_temp."<sup>o</sup>F";
	$insert_keyvalue['network_speed'] = $network_speed."Mbps";
	$insert_keyvalue['os_version'] = $_REQUEST['os_vrsn'];
	$insert_keyvalue['player_vers'] = $_REQUEST['plyr_vrs'];
	$insert_keyvalue['last_reboot'] = $_REQUEST['lst_rbt'];
	$insert_keyvalue['free_hd_space'] = $free_hd_space."Gb";
	$insert_keyvalue['schedule_name'] = $_REQUEST['sch_nme'];
	$insert_keyvalue['last_sched_upload'] = $_REQUEST['lstschd_upld'];
	$insert_keyvalue['schedule_file_size'] = $_REQUEST['sch_fsze']."Gb";
	
	$dbObj->insertRecords('tbl_component_cpu',$insert_keyvalue);
	
	if(count($message)>0) {
		for($msg=0;$msg<count($message); $msg++) {

			$insert_keyvalue_message = array();
			$insert_keyvalue_message['id'] = NULL;
			$insert_keyvalue_message['component_id'] = $_REQUEST['c_id'];
			$insert_keyvalue_message['created_date'] = gmdate("Y-m-d H:i:s");
			$insert_keyvalue_message['message'] = $message[$msg];
			$insert_keyvalue_message['status'] = $color_status[$msg];
			$dbObj->insertRecords('tbl_log_message', $insert_keyvalue_message);
		}
	}
	$insert_keyvalue_state = array();
	
	$insert_keyvalue_state['id'] = NULL;
	$insert_keyvalue_state['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue_state['state'] = $color;
	$insert_keyvalue_state['datetime'] = gmdate("Y-m-d h:i:s");
	
	$dbObj->insertRecords('tbl_component_state',$insert_keyvalue_state);
    echo "Value Stored";

}elseif($datatype == 2) {

	$hvac_in_temp = str_replace("oF","", $_REQUEST['in_tmp']);
	if(is_numeric($hvac_in_temp) && (($hvac_in_temp < 55) || ($hvac_in_temp > 85))) {
		$color_status[] = 2;
		$message[] = "Inside Temperature ".$hvac_in_temp ."<sup>o</sup>F";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}

	$hvac_out_temp = str_replace("oF","", $_REQUEST['out_tmp']);
	if(is_numeric($hvac_out_temp) && (($hvac_out_temp < 55) || ($hvac_out_temp > 85))) {
		$color_status[] = 2;
		$message[] = "Outside Temperature ".$hvac_out_temp ."<sup>o</sup>F";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}
	
	$hvac_water_present = $_REQUEST['water'];
	if($hvac_water_present == "yes") {
		$color_status[] = 2;
		$message[] = "Water Present";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}


	$hvac_inside_humidity = str_replace("%","", $_REQUEST['in_hmdity']);
	if(is_numeric($hvac_inside_humidity) && $hvac_inside_humidity > 20) {
		$color_status[] = 2;
		$message[] = "Inside Humidity ".$hvac_inside_humidity ."%";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}


	
	$red	= "0";
	$yellow	= "0";
	for($i=0;$i<count($stripColor);$i++){
		if($stripColor[$i]=="red"){
			$red	= "1";
		}		
	}
	if($red=="0"){
		for($i=0;$i<count($stripColor);$i++){
			if($stripColor[$i]=="yellow"){
				$yellow	= "1";
			}		
		}
	}

	if($red=="1"){
		$color	= "red";		
	}
	else if($yellow=="1"){
		$color	= "yellow";
	}
	else{
		$color	= "green";
	}
	

	$insert_keyvalue['id'] = NULL;
	$insert_keyvalue['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue['hvac_unit_name'] = $_REQUEST['hvac_unme'];
	$insert_keyvalue['controller_firmware_ver'] = $_REQUEST['clr_frm'];
	$insert_keyvalue['ac_status'] = $_REQUEST['ac_sts'];
	$insert_keyvalue['heater_status'] = $_REQUEST['hetr_sts'];
	$insert_keyvalue['water_present'] = $hvac_water_present;
	$insert_keyvalue['inside_temp'] = $hvac_in_temp."<sup>o</sup>F";
	$insert_keyvalue['outside_temp'] = $hvac_out_temp."<sup>o</sup>F";
	$insert_keyvalue['inside_humidity'] = $hvac_inside_humidity."%";
	
	$dbObj->insertRecords('tbl_component_hvac',$insert_keyvalue);

	if(count($message)>0) {
		for($msg=0;$msg<count($message); $msg++) {
			$insert_keyvalue_message = array();
			$insert_keyvalue_message['id'] = NULL;
			$insert_keyvalue_message['component_id'] = $_REQUEST['c_id'];
			$insert_keyvalue_message['created_date'] = gmdate("Y-m-d h:i:s");
			$insert_keyvalue_message['message'] = $message[$msg];
			$insert_keyvalue_message['status'] = $color_status[$msg];
			$dbObj->insertRecords('tbl_log_message', $insert_keyvalue_message);
		}
	}
	$insert_keyvalue_state = array();
	
	$insert_keyvalue_state['id'] = NULL;
	$insert_keyvalue_state['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue_state['state'] = $color;
	$insert_keyvalue_state['datetime'] = gmdate("Y-m-d h:i:s");
	
	$dbObj->insertRecords('tbl_component_state',$insert_keyvalue_state);
	echo "Value Stored";

}elseif($datatype == 3){

	$current_status = $_REQUEST['crnt_sts'];
	if($current_status== "offline" || $current_status== "") {
		$color_status[] = 3;
		$message[] = "shut down";
		$stripColor[]	= "red";
		$color = "red";
	}	

	$dis_temp = str_replace("oF","", $_REQUEST['ds_tmp']);
	if(is_numeric($dis_temp) && (($dis_temp < 60) || ($dis_temp > 90))) {
		$color_status[] = 2;
		$message[] = "display temperature ".$dis_temp ."<sup>o</sup>F";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}

	$dis_bk_temp = str_replace("oF","", $_REQUEST['bk_tmp']);
	if(is_numeric($dis_bk_temp) && (($dis_bk_temp < 60) || ($dis_bk_temp > 90))) {
		$color_status[] = 2;
		$message[] = "backlight temperature ".$dis_bk_temp ."<sup>o</sup>F";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}

	$dis_ctrl_temp = str_replace("oF","", $_REQUEST['ctrlbrd_tmp']);
	if(is_numeric($dis_ctrl_temp) && (($dis_ctrl_temp < 65) || ($dis_ctrl_temp > 95))) {
		$color_status[] = 2;
		$message[] = "control board temperature ".$dis_ctrl_temp ."<sup>o</sup>F";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}
	

	$dis_dty_cycle = str_replace("%","", $_REQUEST['dty_cyle']);
	if($dis_dty_cycle < 15) {
		$color_status[] = 2;
		$message[] = "Duty Cycle ".$dis_dty_cycle ."%";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}

	$dis_brght = str_replace("%","", $_REQUEST['brght']);
	if(is_numeric($dis_brght)){
		if(($dis_brght >5) && ($dis_brght < 20)){
		$color_status[] = 2;
		$message[] = "Brightness ".$dis_brght ."%";
		$stripColor[]	= "yellow";
		$color = "yellow";
	}
	elseif($dis_brght<5){
		$color_status[] = 3;
		$message[] = "Brightness ".$dis_brght." %";
		$color = "red";
		$stripColor[]	= "red";
}
}

// Red - <5%
// Yellow - >5% & <20%
// Green - >20%
// This is for finding out last ping.

 	  $sql = $dbObj->executeQuery("SELECT last_ping FROM tbl_component_display where component_id='".$_REQUEST['c_id']."' ORDER BY id DESC LIMIT 1", true);
	//echo "<pre>";print_r($sql);exit;

  	//echo  gmdate($sql['last_ping'])."<br>".gmdate("Y-m-d h:i:s");
	if($sql['last_ping']){
	$currentPingTime 	= strtotime(gmdate("Y-m-d H:i:s"));
	$lastPingTime		= strtotime($sql['last_ping']) ;
	$pingDifference		= $currentPingTime - $lastPingTime ;
	$pingDifference		= round($pingDifference/60);
	}else{
		$pingDifference = 0;
	}
	//minutes to hour minute	
	$hours          = floor($pingDifference / 60);
	$decimalMinutes = $pingDifference - floor($pingDifference/60) * 60;

	# Put it together.
	$hoursMinutes = sprintf("%dH%02.0fM", $hours, $decimalMinutes);
//
	if($pingDifference >=5 ) {
		if($pingDifference <= 10){
			$color_status[] = 2;
			$message[] =  "last ping was : ".$hoursMinutes." ago";
			$color = "yellow";
			$stripColor[]	= "yellow";
		}
		else{
			$color_status[] = 3;
			$message[] ="last ping was : ".$hoursMinutes." ago";
			$color = "red";
			$stripColor[]	= "red";
		}		
	}
		

	$red	= "0";
	$yellow	= "0";
	for($i=0;$i<count($stripColor);$i++){
		if($stripColor[$i]=="red"){
			$red	= "1";
		}		
	}
	if($red=="0"){
		for($i=0;$i<count($stripColor);$i++){
			if($stripColor[$i]=="yellow"){
				$yellow	= "1";
			}		
		}
	}

	if($red=="1"){
		$color	= "red";		
	}
	else if($yellow=="1"){
		$color	= "yellow";
	}
	else{
		$color	= "green";
	}
	


	$insert_keyvalue['id'] = NULL;
	$insert_keyvalue['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue['display_type'] = $_REQUEST['ds_tpe'];
	$insert_keyvalue['display_name'] = $_REQUEST['ds_nme'];
	$insert_keyvalue['ip_address'] = $_REQUEST['ip_ad'];
	$insert_keyvalue['last_ping'] = gmdate("Y-m-d H:i:s");
	$insert_keyvalue['current_status'] = $current_status;
	$insert_keyvalue['display_temp'] = $dis_temp."<sup>o</sup>F";
	$insert_keyvalue['backlight_temp'] = $dis_bk_temp."<sup>o</sup>F";
	$insert_keyvalue['ctrl_board_temp'] = $dis_ctrl_temp."<sup>o</sup>F";
	$insert_keyvalue['duty_cycle'] = $dis_dty_cycle."%";
	$insert_keyvalue['ambient_light'] = $_REQUEST['ambnt_lght']."%";
	$insert_keyvalue['brightness'] =$dis_brght."%";
	$insert_keyvalue['input_source'] = $_REQUEST['inpt_srce'];
//echo "<pre>";print_r($_REQUEST['ambnt_lght']."%");exit;
	
	$dbObj->insertRecords('tbl_component_display',$insert_keyvalue);

	if(count($message)>0) {
		for($msg=0;$msg<count($message); $msg++) {

			$insert_keyvalue_message = array();
			$insert_keyvalue_message['id'] = NULL;
			$insert_keyvalue_message['component_id'] = $_REQUEST['c_id'];
			$insert_keyvalue_message['created_date'] = gmdate("Y-m-d h:i:s");
			$insert_keyvalue_message['message'] = $message[$msg];
			$insert_keyvalue_message['status'] = $color_status[$msg];
			//echo "<pre>";print_r($color_status[$msg]);
			$dbObj->insertRecords('tbl_log_message', $insert_keyvalue_message);
		}
	}
	$insert_keyvalue_state = array();
	
	$insert_keyvalue_state['id'] = NULL;
	$insert_keyvalue_state['component_id'] = $_REQUEST['c_id'];
	$insert_keyvalue_state['state'] = $color;
	$insert_keyvalue_state['datetime'] = gmdate("Y-m-d h:i:s");
	
	$dbObj->insertRecords('tbl_component_state',$insert_keyvalue_state);
	
	echo "Value Stored";

}



?>
