-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 21, 2012 at 07:48 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_display`
--

DROP TABLE IF EXISTS `tbl_component_display`;
CREATE TABLE IF NOT EXISTS `tbl_component_display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `display_type` int(2) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `last_ping` datetime NOT NULL,
  `current_status` varchar(50) NOT NULL,
  `display_temp` varchar(50) NOT NULL,
  `backlight_temp` varchar(50) NOT NULL,
  `ctrl_board_temp` varchar(50) NOT NULL,
  `duty_cycle` varchar(100) NOT NULL,
  `ambient_light` varchar(100) NOT NULL,
  `brightness` varchar(100) NOT NULL,
  `input_source` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tbl_component_display`
--

INSERT INTO `tbl_component_display` (`id`, `component_id`, `display_type`, `display_name`, `ip_address`, `last_ping`, `current_status`, `display_temp`, `backlight_temp`, `ctrl_board_temp`, `duty_cycle`, `ambient_light`, `brightness`, `input_source`) VALUES
(8, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:24:10', 'offline', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(9, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:32:00', 'online', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(10, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:41:14', 'online', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(11, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:41:57', 'online', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(12, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:42:01', 'online', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(13, 3, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:42:16', 'online', '83 oF', '102 oF', '89 oF', '23', '83', '40', 'DVI1'),
(20, 3, 0, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 14:56:54', 'offline', '83 oF', '73 oF', '89 oF', '23', '83', '40', 'DVI1'),
(21, 3, 1, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 15:00:14', 'offline', '83 oF', '73 oF', '89 oF', '23', '83', '40', 'DVI1'),
(22, 3, 1, 'Upper Left Horiz - 07ULH', '123.142.0.2', '2012-03-15 15:02:19', 'online', '83 oF', '73 oF', '89 oF', '23', '83', '40', 'DVI1');
