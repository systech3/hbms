-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 09, 2012 at 09:02 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_components`
--

DROP TABLE IF EXISTS `tbl_components`;
CREATE TABLE IF NOT EXISTS `tbl_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(100) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `tbl_components`
--

INSERT INTO `tbl_components` (`id`, `component_name`, `device_id`) VALUES
(1, 'CPUH', 1),
(2, 'CPUV', 1),
(3, 'Displays', 1),
(4, 'HVAC', 1),
(5, 'CPUH', 2),
(6, 'CPUV', 2),
(7, 'Displays', 2),
(8, 'HVAC', 2),
(9, 'CPUH', 3),
(10, 'CPUV', 3),
(11, 'Displays', 3),
(12, 'HVAC', 3),
(13, 'CPUH', 4),
(14, 'CPUV', 4),
(15, 'Displays', 4),
(16, 'HVAC', 4),
(17, 'CPUH', 5),
(18, 'CPUV', 5),
(19, 'Displays', 5),
(20, 'HVAC', 5),
(21, 'CPUH', 6),
(22, 'CPUV', 6),
(23, 'Displays', 6),
(24, 'HVAC', 6),
(25, 'CPUH', 7),
(26, 'CPUV', 7),
(27, 'Displays', 7),
(28, 'HVAC', 7),
(29, 'CPUH', 8),
(30, 'CPUV', 8),
(31, 'Displays', 8),
(32, 'HVAC', 8),
(33, 'CPUH', 9),
(34, 'CPUV', 9),
(35, 'Displays', 9),
(36, 'HVAC', 9),
(37, 'CPUH', 10),
(38, 'CPUV', 10),
(39, 'Displays', 10),
(40, 'HVAC', 10),
(41, 'CPUH', 11),
(42, 'CPUV', 11),
(43, 'Displays', 11),
(44, 'HVAC', 11),
(45, 'CPUH', 12),
(46, 'CPUV', 12),
(47, 'Displays', 12),
(48, 'HVAC', 12),
(49, 'CPUH', 13),
(50, 'CPUV', 13),
(51, 'Displays', 13),
(52, 'HVAC', 13),
(53, 'CPUH', 14),
(54, 'CPUV', 14),
(55, 'Displays', 14),
(56, 'HVAC', 14);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_cpu`
--

DROP TABLE IF EXISTS `tbl_component_cpu`;
CREATE TABLE IF NOT EXISTS `tbl_component_cpu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `component_type` int(2) NOT NULL,
  `cpu_name` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `last_ping` time NOT NULL,
  `current_status` varchar(50) NOT NULL,
  `cpu_temp` varchar(50) NOT NULL,
  `network_speed` varchar(50) NOT NULL,
  `os_version` varchar(50) NOT NULL,
  `player_vers` varchar(50) NOT NULL,
  `last_reboot` varchar(50) NOT NULL,
  `free_hd_space` varchar(255) NOT NULL,
  `schedule_name` varchar(50) NOT NULL,
  `last_sched_upload` varchar(50) NOT NULL,
  `schedule_file_size` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245 ;

--
-- Dumping data for table `tbl_component_cpu`
--

INSERT INTO `tbl_component_cpu` (`id`, `component_id`, `component_type`, `cpu_name`, `ip_address`, `last_ping`, `current_status`, `cpu_temp`, `network_speed`, `os_version`, `player_vers`, `last_reboot`, `free_hd_space`, `schedule_name`, `last_sched_upload`, `schedule_file_size`) VALUES
(244, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '3000oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(243, 1, 0, 'CPU009', '123.142.0.2', '12:20:00', 'online', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(242, 1, 0, 'CPU009', '123.142.0.2', '12:20:00', 'online', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(241, 1, 0, 'CPU009', '123.142.0.2', '05:35:00', 'online', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(240, 1, 0, 'CPU009', '123.142.0.2', '01:35:00', 'online', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(239, 1, 0, 'CPU009', '123.142.0.2', '01:35:00', 'offline', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(238, 1, 0, 'CPU009', '123.142.0.2', '01:35:00', 'offline', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(237, 1, 0, 'CPU009', '123.142.0.2', '01:35:00', 'offline', '83oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(236, 1, 0, 'CPU009', '123.142.0.2', '01:35:00', 'offline', '30oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(235, 2, 0, 'CPU009', '123.142.0.2', '00:34:00', 'offline', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(234, 2, 0, 'CPU009', '123.142.0.2', '00:34:00', 'online', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(233, 2, 0, 'CPU009', '123.142.0.2', '11:34:00', 'online', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(232, 2, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(231, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(230, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'online', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(229, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '30oF', '899', '10.7.1', '2.8.1', '12:04', '30Gb', 'SCHED123', '12:30', 'Gb'),
(228, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '30oF', '6Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(227, 1, 0, 'CPU009', '123.142.0.2', '11:34:00', 'offline', '30oF', '2Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(226, 1, 0, 'CPU009', '123.142.0.2', '05:45:00', 'offline', '30oF', '35Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(225, 1, 0, 'CPU009', '123.142.0.2', '05:45:00', 'offline', '83oF', '35Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(224, 1, 0, 'CPU009', '123.142.0.2', '05:19:00', 'online', '50oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(223, 1, 0, 'CPU009', '123.142.0.2', '05:19:00', 'online', '30oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(222, 1, 0, 'CPU009', '123.142.0.2', '05:19:00', 'online', '30oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb'),
(221, 1, 0, 'CPU009', '123.142.0.2', '05:17:00', 'offline', '30oF', '9Mbps', '10.7.1', '2.8.1', '12:04', 'Gb', 'SCHED123', '12:30', 'Gb');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_display`
--

DROP TABLE IF EXISTS `tbl_component_display`;
CREATE TABLE IF NOT EXISTS `tbl_component_display` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `display_type` int(2) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `last_ping` time NOT NULL,
  `current_status` varchar(50) NOT NULL,
  `display_temp` varchar(50) NOT NULL,
  `backlight_temp` varchar(50) NOT NULL,
  `ctrl_board_temp` varchar(50) NOT NULL,
  `duty_cycle` int(11) NOT NULL,
  `ambient_light` int(11) NOT NULL,
  `brightness` int(11) NOT NULL,
  `input_source` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `tbl_component_display`
--

INSERT INTO `tbl_component_display` (`id`, `component_id`, `display_type`, `display_name`, `ip_address`, `last_ping`, `current_status`, `display_temp`, `backlight_temp`, `ctrl_board_temp`, `duty_cycle`, `ambient_light`, `brightness`, `input_source`) VALUES
(1, 3, 0, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '102oF', '80oF', '89oF', 23, 83, 40, 'DVI1'),
(2, 3, 1, 'Upper Right Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '102oF', '80oF', '89oF', 23, 83, 40, 'DVI1'),
(3, 3, 2, 'Bottom Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '102oF', '80oF', '89oF', 23, 83, 40, 'DVI1'),
(4, 3, 3, 'Bottom Right Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '102oF', '80oF', '89oF', 23, 83, 40, 'DVI1'),
(5, 3, 4, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '102oF', '80oF', '89oF', 23, 83, 40, 'DVI1'),
(6, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83oF', '102oF', '89oF', 23, 83, 40, 'DVI1'),
(7, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83oF', '102oF', '89oF', 23, 83, 40, 'DVI1'),
(8, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83 oF', '102 oF', '89 oF', 23, 83, 40, 'DVI1'),
(9, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83 oF', '102 oF', '89 oF', 23, 83, 40, 'DVI1'),
(10, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83 oF', '102 oF', '89 oF', 23, 83, 40, 'DVI1'),
(11, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83 oF', '102 oF', '89 oF', 23, 83, 40, 'DVI1'),
(12, 9, 3, 'Upper Left Horiz - 07ULH', '123.142.0.2', '10:52:00', 'online', '83 oF', '102 oF', '89 oF', 23, 83, 40, 'DVI1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_hvac`
--

DROP TABLE IF EXISTS `tbl_component_hvac`;
CREATE TABLE IF NOT EXISTS `tbl_component_hvac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `hvac_unit_name` varchar(255) NOT NULL,
  `controller_firmware_ver` varchar(255) NOT NULL,
  `ac_status` varchar(255) NOT NULL,
  `heater_status` varchar(255) NOT NULL,
  `water_present` varchar(255) NOT NULL,
  `inside_temp` varchar(255) NOT NULL,
  `outside_temp` varchar(50) NOT NULL,
  `inside_humidity` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `tbl_component_hvac`
--

INSERT INTO `tbl_component_hvac` (`id`, `component_id`, `hvac_unit_name`, `controller_firmware_ver`, `ac_status`, `heater_status`, `water_present`, `inside_temp`, `outside_temp`, `inside_humidity`) VALUES
(5, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83o', '83o', ''),
(4, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83o', '83o', '10%'),
(6, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83o', '83o', ''),
(7, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83o', '83o', ''),
(8, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83o', '83', '10%'),
(9, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83ooF', '83oF', '10%'),
(10, 9, 'HVAC003', '1.2.3', 'on', 'on', 'no', '83ooF', '83oF', '10%'),
(11, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10%'),
(12, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10%'),
(13, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '30'),
(17, 9, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10'),
(19, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10'),
(20, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '90oF', '90oF', '10'),
(32, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10'),
(33, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '10'),
(34, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '83oF', '83oF', '15'),
(35, 4, 'HVAC003', '1.2.3', 'on', 'on', 'yes', '900oF', '900oF', '25'),
(36, 4, 'HVAC003', '1.2.3', 'on', 'on', 'no', '83oF', '83oF', '15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_state`
--

DROP TABLE IF EXISTS `tbl_component_state`;
CREATE TABLE IF NOT EXISTS `tbl_component_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `state` enum('green','yellow','red','blue') NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `tbl_component_state`
--

INSERT INTO `tbl_component_state` (`id`, `component_id`, `state`, `datetime`) VALUES
(1, 1, 'yellow', '2012-03-09 05:16:55'),
(2, 1, 'red', '2012-03-09 05:17:13'),
(3, 1, 'red', '2012-03-09 05:17:51'),
(4, 1, 'red', '2012-03-09 05:19:29'),
(5, 1, 'yellow', '2012-03-09 05:20:28'),
(6, 1, 'yellow', '2012-03-09 05:22:45'),
(7, 1, 'green', '2012-03-09 05:23:17'),
(8, 1, 'red', '2012-03-09 05:24:35'),
(9, 1, 'red', '2012-03-09 05:25:02'),
(10, 1, 'red', '2012-03-09 05:25:35'),
(11, 1, 'red', '2012-03-09 05:26:05'),
(12, 1, 'red', '2012-03-09 05:28:18'),
(13, 1, 'yellow', '2012-03-09 05:28:45'),
(14, 1, 'red', '2012-03-09 05:29:19'),
(15, 2, 'red', '2012-03-09 05:31:29'),
(16, 2, 'yellow', '2012-03-09 05:31:45'),
(17, 3, 'red', '2012-03-09 05:34:14'),
(18, 3, 'yellow', '2012-03-09 05:34:40'),
(19, 2, 'red', '2012-03-09 05:35:03'),
(20, 3, 'red', '2012-03-09 05:35:15'),
(21, 3, 'red', '2012-03-09 05:35:46'),
(22, 3, 'yellow', '2012-03-09 05:36:01'),
(23, 2, 'red', '2012-03-09 05:36:16'),
(24, 3, 'yellow', '2012-03-09 05:36:25'),
(25, 3, 'red', '2012-03-09 05:36:53'),
(26, 4, 'yellow', '2012-03-09 05:38:50'),
(27, 3, 'yellow', '2012-03-09 05:39:23'),
(28, 3, 'red', '2012-03-09 05:40:49'),
(29, 4, 'yellow', '2012-03-09 05:42:07'),
(30, 1, 'red', '2012-03-09 05:44:47'),
(31, 1, 'red', '2012-03-09 05:45:00'),
(32, 1, 'red', '2012-03-09 05:48:14'),
(33, 3, 'red', '2012-03-09 05:48:17'),
(34, 1, 'red', '2012-03-09 05:48:48'),
(35, 1, 'red', '2012-03-09 05:48:58'),
(36, 1, 'red', '2012-03-09 05:49:12'),
(37, 1, 'yellow', '2012-03-09 05:49:30'),
(38, 1, 'yellow', '2012-03-09 05:54:13'),
(39, 4, 'yellow', '2012-03-09 07:19:49'),
(40, 4, 'yellow', '2012-03-09 07:20:55'),
(41, 4, 'green', '2012-03-09 07:21:43'),
(42, 1, 'red', '2012-03-09 08:04:44'),
(43, 3, 'red', '2012-03-09 08:07:36'),
(44, 3, 'red', '2012-03-09 08:30:39'),
(45, 3, 'red', '2012-03-09 08:35:47'),
(46, 3, 'red', '2012-03-09 08:36:20'),
(47, 3, 'red', '2012-03-09 08:37:05'),
(48, 3, 'red', '2012-03-09 08:37:42'),
(49, 3, 'red', '2012-03-09 08:37:55'),
(50, 3, 'red', '2012-03-09 08:39:48'),
(51, 3, 'red', '2012-03-09 08:40:26'),
(52, 3, 'green', '2012-03-09 08:40:49'),
(53, 3, 'green', '2012-03-09 08:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_device`
--

DROP TABLE IF EXISTS `tbl_device`;
CREATE TABLE IF NOT EXISTS `tbl_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_device`
--

INSERT INTO `tbl_device` (`id`, `device_name`) VALUES
(1, 'u01 47 / Bway'),
(2, 'u01 47 / Bway'),
(3, 'u01 47 / Bway'),
(4, 'u01 47 / Bway'),
(5, 'u01 47 / Bway'),
(6, 'u01 47 / Bway'),
(7, 'u01 47 / Bway'),
(8, 'u01 47 / Bway'),
(9, 'u01 47 / Bway'),
(10, 'u01 47 / Bway'),
(11, 'u01 47 / Bway'),
(12, 'u01 47 / Bway'),
(13, 'u01 47 / Bway'),
(14, 'u01 47 / Bway');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_message`
--

DROP TABLE IF EXISTS `tbl_log_message`;
CREATE TABLE IF NOT EXISTS `tbl_log_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Dumping data for table `tbl_log_message`
--

INSERT INTO `tbl_log_message` (`id`, `component_id`, `created_date`, `message`, `status`) VALUES
(1, 1, '2012-03-09 11:46:55', 'temperature 30', 2),
(2, 1, '2012-03-09 11:46:55', 'last ping test 6', 2),
(3, 1, '2012-03-09 11:47:13', 'temperature 30', 2),
(4, 1, '2012-03-09 11:47:13', 'shut down', 3),
(5, 1, '2012-03-09 11:47:13', 'last ping test 6', 2),
(6, 1, '2012-03-09 11:47:51', 'temperature 30', 2),
(7, 1, '2012-03-09 11:47:51', 'shut down', 3),
(8, 1, '2012-03-09 11:49:29', 'temperature 30', 2),
(9, 1, '2012-03-09 11:49:29', 'shut down', 3),
(10, 1, '2012-03-09 11:50:28', 'temperature 30', 2),
(11, 1, '2012-03-09 11:52:45', 'temperature 30', 2),
(12, 1, '2012-03-09 11:54:35', 'temperature 83', 2),
(13, 1, '2012-03-09 11:54:35', 'shut down', 3),
(14, 1, '2012-03-09 11:55:02', 'temperature 30', 2),
(15, 1, '2012-03-09 11:55:02', 'shut down', 3),
(16, 1, '2012-03-09 11:55:35', 'temperature 30', 2),
(17, 1, '2012-03-09 11:55:35', 'shut down', 3),
(18, 1, '2012-03-09 11:56:05', 'temperature 30', 2),
(19, 1, '2012-03-09 11:56:05', 'shut down', 3),
(20, 1, '2012-03-09 11:58:18', 'temperature 30', 2),
(21, 1, '2012-03-09 11:58:18', 'shut down', 3),
(22, 1, '2012-03-09 11:58:18', 'Free HD space 30 Gb', 2),
(23, 1, '2012-03-09 11:58:45', 'temperature 30', 2),
(24, 1, '2012-03-09 11:58:45', 'Free HD space 30 Gb', 2),
(25, 1, '2012-03-09 11:59:19', 'temperature 30', 2),
(26, 1, '2012-03-09 11:59:19', 'shut down', 3),
(27, 1, '2012-03-09 11:59:19', 'Free HD space 30 Gb', 2),
(28, 2, '2012-03-09 12:01:29', 'temperature 30', 2),
(29, 2, '2012-03-09 12:01:29', 'shut down', 3),
(30, 2, '2012-03-09 12:01:29', 'Free HD space 30 Gb', 2),
(31, 2, '2012-03-09 12:01:45', 'temperature 30', 2),
(32, 2, '2012-03-09 12:01:45', 'Free HD space 30 Gb', 2),
(33, 3, '2012-03-09 05:34:14', 'shut down', 3),
(34, 3, '2012-03-09 05:34:14', 'display temperature 100', 2),
(35, 3, '2012-03-09 05:34:14', 'backlight temperature 100', 2),
(36, 3, '2012-03-09 05:34:14', 'Duty Cycle 14', 2),
(37, 3, '2012-03-09 05:34:40', 'display temperature 100', 2),
(38, 3, '2012-03-09 05:34:40', 'backlight temperature 100', 2),
(39, 3, '2012-03-09 05:34:40', 'Duty Cycle 14', 2),
(40, 2, '2012-03-09 12:05:03', 'temperature 30', 2),
(41, 2, '2012-03-09 12:05:03', 'Free HD space 30 Gb', 2),
(42, 2, '2012-03-09 12:05:03', 'last ping test 22187525', 3),
(43, 3, '2012-03-09 05:35:15', 'shut down', 3),
(44, 3, '2012-03-09 05:35:15', 'display temperature 100', 2),
(45, 3, '2012-03-09 05:35:15', 'backlight temperature 100', 2),
(46, 3, '2012-03-09 05:35:15', 'Duty Cycle 14', 2),
(47, 3, '2012-03-09 05:35:46', 'shut down', 3),
(48, 3, '2012-03-09 05:35:46', 'display temperature 100', 2),
(49, 3, '2012-03-09 05:35:46', 'Duty Cycle 14', 2),
(50, 3, '2012-03-09 05:36:01', 'display temperature 100', 2),
(51, 3, '2012-03-09 05:36:01', 'Duty Cycle 14', 2),
(52, 2, '2012-03-09 12:06:16', 'temperature 30', 2),
(53, 2, '2012-03-09 12:06:16', 'shut down', 3),
(54, 2, '2012-03-09 12:06:16', 'Free HD space 30 Gb', 2),
(55, 2, '2012-03-09 12:06:16', 'last ping test 22187526', 3),
(56, 3, '2012-03-09 05:36:25', 'display temperature 100', 2),
(57, 3, '2012-03-09 05:36:25', 'Duty Cycle 14', 2),
(58, 3, '2012-03-09 05:36:53', 'shut down', 3),
(59, 3, '2012-03-09 05:36:53', 'display temperature 100', 2),
(60, 3, '2012-03-09 05:36:53', 'Duty Cycle 14', 2),
(61, 4, '2012-03-09 05:38:50', 'Water Present', 2),
(62, 3, '2012-03-09 05:39:23', 'display temperature 100', 2),
(63, 3, '2012-03-09 05:39:23', 'Duty Cycle 14', 2),
(64, 3, '2012-03-09 12:10:49', 'display temperature 100', 2),
(65, 3, '2012-03-09 12:10:49', 'Duty Cycle 14', 2),
(66, 3, '2012-03-09 12:10:49', 'last ping test 265', 3),
(67, 4, '2012-03-09 12:12:07', 'Water Present', 2),
(68, 1, '2012-03-09 12:14:47', 'temperature 30oF', 2),
(69, 1, '2012-03-09 12:14:47', 'shut down', 3),
(70, 1, '2012-03-09 12:14:47', 'last ping test 250', 3),
(71, 1, '2012-03-09 12:15:00', 'temperature 83oF', 2),
(72, 1, '2012-03-09 12:15:00', 'shut down', 3),
(73, 1, '2012-03-09 12:15:00', 'last ping test 250', 3),
(74, 1, '2012-03-09 12:18:14', 'temperature 83oF', 2),
(75, 1, '2012-03-09 12:18:14', 'shut down', 3),
(76, 1, '2012-03-09 12:18:14', 'last ping test 253', 3),
(77, 3, '2012-03-09 12:18:17', 'display temperature 100oF', 2),
(78, 3, '2012-03-09 12:18:17', 'Duty Cycle 14%', 2),
(79, 3, '2012-03-09 12:18:17', 'last ping test 272', 3),
(80, 1, '2012-03-09 12:18:48', 'temperature 83oF', 2),
(81, 1, '2012-03-09 12:18:48', 'shut down', 3),
(82, 1, '2012-03-09 12:18:48', 'last ping test 254', 3),
(83, 1, '2012-03-09 12:18:58', 'temperature 83oF', 2),
(84, 1, '2012-03-09 12:18:58', 'last ping test 254', 3),
(85, 1, '2012-03-09 12:19:12', 'temperature 83oF', 2),
(86, 1, '2012-03-09 12:19:12', 'last ping test 14', 3),
(87, 1, '2012-03-09 12:19:30', 'temperature 83oF', 2),
(88, 1, '2012-03-09 12:24:13', 'temperature 83oF', 2),
(89, 4, '2012-03-09 01:49:49', 'Water Present', 2),
(90, 4, '2012-03-09 01:50:55', 'Inside Temperature 900oF', 2),
(91, 4, '2012-03-09 01:50:55', 'Outside Temperature 900oF', 2),
(92, 4, '2012-03-09 01:50:55', 'Water Present', 2),
(93, 4, '2012-03-09 01:50:55', 'Inside Humidity 25%', 2),
(94, 1, '2012-03-09 02:34:44', 'temperature 3000<sup>o</sup>F', 2),
(95, 1, '2012-03-09 02:34:44', 'shut down', 3),
(96, 1, '2012-03-09 02:34:44', 'Free HD space 30 Gb', 2),
(97, 3, '2012-03-09 02:37:36', 'shut down', 3),
(98, 3, '2012-03-09 02:37:36', 'display temperature 10000<sup>o</sup>F', 2),
(99, 3, '2012-03-09 02:37:36', 'backlight temperature 999100<sup>o</sup>F', 2),
(100, 3, '2012-03-09 02:37:36', 'Duty Cycle 14%', 2),
(101, 3, '2012-03-09 02:37:36', 'last ping test 412', 3),
(102, 3, '2012-03-09 03:00:39', 'shut down', 3),
(103, 3, '2012-03-09 03:00:39', 'display temperature 10000<sup>o</sup>F', 2),
(104, 3, '2012-03-09 03:00:39', 'backlight temperature 999100<sup>o</sup>F', 2),
(105, 3, '2012-03-09 03:00:39', 'Duty Cycle 14%', 2),
(106, 3, '2012-03-09 03:00:39', 'last ping 10 mins since435', 3),
(107, 3, '2012-03-09 03:05:47', 'shut down', 3),
(108, 3, '2012-03-09 03:05:47', 'display temperature 10000<sup>o</sup>F', 2),
(109, 3, '2012-03-09 03:05:47', 'backlight temperature 999100<sup>o</sup>F', 2),
(110, 3, '2012-03-09 03:05:47', 'Duty Cycle 14%', 2),
(111, 3, '2012-03-09 03:05:47', 'last ping was : 440', 3),
(112, 3, '2012-03-09 03:06:20', 'shut down', 3),
(113, 3, '2012-03-09 03:06:20', 'display temperature 10000<sup>o</sup>F', 2),
(114, 3, '2012-03-09 03:06:20', 'backlight temperature 999100<sup>o</sup>F', 2),
(115, 3, '2012-03-09 03:06:20', 'Duty Cycle 14%', 2),
(116, 3, '2012-03-09 03:06:20', 'last ping was : 26420', 3),
(117, 3, '2012-03-09 03:07:05', 'shut down', 3),
(118, 3, '2012-03-09 03:07:05', 'display temperature 10000<sup>o</sup>F', 2),
(119, 3, '2012-03-09 03:07:05', 'backlight temperature 999100<sup>o</sup>F', 2),
(120, 3, '2012-03-09 03:07:05', 'Duty Cycle 14%', 2),
(121, 3, '2012-03-09 03:07:05', 'last ping was : 7', 3),
(122, 3, '2012-03-09 03:07:42', 'shut down', 3),
(123, 3, '2012-03-09 03:07:42', 'display temperature 10000<sup>o</sup>F', 2),
(124, 3, '2012-03-09 03:07:42', 'backlight temperature 999100<sup>o</sup>F', 2),
(125, 3, '2012-03-09 03:07:42', 'Duty Cycle 14%', 2),
(126, 3, '2012-03-09 03:07:42', 'last ping was : 7Minutes ago', 3),
(127, 3, '2012-03-09 03:07:55', 'shut down', 3),
(128, 3, '2012-03-09 03:07:55', 'display temperature 10000<sup>o</sup>F', 2),
(129, 3, '2012-03-09 03:07:55', 'backlight temperature 999100<sup>o</sup>F', 2),
(130, 3, '2012-03-09 03:07:55', 'Duty Cycle 14%', 2),
(131, 3, '2012-03-09 03:07:55', 'last ping was : 7Minutes ago', 3),
(132, 3, '2012-03-09 03:09:48', 'last ping was : 6Minutes ago', 3),
(133, 3, '2012-03-09 03:10:26', 'last ping was : 6Minutes ago', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `last_login_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `email`, `display_name`, `last_login_date`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 'super user', '2012-02-25 12:06:30'),
(2, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.com', 'test user', '0000-00-00 00:00:00');
