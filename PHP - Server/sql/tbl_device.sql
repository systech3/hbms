-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 24, 2012 at 02:49 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_device`
--

DROP TABLE IF EXISTS `tbl_device`;
CREATE TABLE IF NOT EXISTS `tbl_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_device`
--

INSERT INTO `tbl_device` (`id`, `device_name`) VALUES
(1, 'u01 48 / Bway'),
(2, 'u02 42 / 7th'),
(3, 'u03 43 / Bway'),
(4, 'u04 41 / Bway'),
(5, 'u05 44N / Bway'),
(6, 'u06 46 / Bway'),
(7, 'u07 49 / Bway'),
(8, 'u08 44S / Bway'),
(9, 'u01 47 / Bway'),
(10, 'u01 47 / Bway'),
(11, 'u01 47 / Bway'),
(12, 'u01 47 / Bway'),
(13, 'u01 47 / Bway'),
(14, 'u01 47 / Bway');
