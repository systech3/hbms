-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2012 at 06:42 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_hvac`
--

DROP TABLE IF EXISTS `tbl_component_hvac`;
CREATE TABLE IF NOT EXISTS `tbl_component_hvac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `cpu_name` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `last_ping` datetime NOT NULL,
  `current_status` varchar(255) NOT NULL,
  `cpu_temp` varchar(50) NOT NULL,
  `network_speed` varchar(50) NOT NULL,
  `os_version` varchar(50) NOT NULL,
  `player_vers` varchar(255) NOT NULL,
  `last_reboot` varchar(255) NOT NULL,
  `schedule_name` varchar(255) NOT NULL,
  `last_sched_upload` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_component_hvac`
--

INSERT INTO `tbl_component_hvac` (`id`, `component_id`, `cpu_name`, `ip_address`, `last_ping`, `current_status`, `cpu_temp`, `network_speed`, `os_version`, `player_vers`, `last_reboot`, `schedule_name`, `last_sched_upload`) VALUES
(1, 4, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '28', '12', '"SCHED123"', '12:30a 01/03/12');
