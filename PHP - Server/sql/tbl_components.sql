-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2012 at 06:15 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_components`
--

DROP TABLE IF EXISTS `tbl_components`;
CREATE TABLE IF NOT EXISTS `tbl_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_name` varchar(100) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `tbl_components`
--

INSERT INTO `tbl_components` (`id`, `component_name`, `device_id`) VALUES
(1, 'CPUH', 1),
(2, 'CPUV', 1),
(3, 'Displays', 1),
(4, 'HVAC', 1),
(5, 'CPUH', 2),
(6, 'CPUV', 2),
(7, 'Displays', 2),
(8, 'HVAC', 2),
(9, 'CPUH', 3),
(10, 'CPUV', 3),
(11, 'Displays', 3),
(12, 'HVAC', 3),
(13, 'CPUH', 4),
(14, 'CPUV', 4),
(15, 'Displays', 4),
(16, 'HVAC', 4),
(17, 'CPUH', 5),
(18, 'CPUV', 5),
(19, 'Displays', 5),
(20, 'HVAC', 5),
(21, 'CPUH', 6),
(22, 'CPUV', 6),
(23, 'Displays', 6),
(24, 'HVAC', 6),
(25, 'CPUH', 7),
(26, 'CPUV', 7),
(27, 'Displays', 7),
(28, 'HVAC', 7),
(29, 'CPUH', 8),
(30, 'CPUV', 8),
(31, 'Displays', 8),
(32, 'HVAC', 8),
(33, 'CPUH', 9),
(34, 'CPUV', 9),
(35, 'Displays', 9),
(36, 'HVAC', 9),
(37, 'CPUH', 10),
(38, 'CPUV', 10),
(39, 'Displays', 10),
(40, 'HVAC', 10),
(41, 'CPUH', 11),
(42, 'CPUV', 11),
(43, 'Displays', 11),
(44, 'HVAC', 11),
(45, 'CPUH', 12),
(46, 'CPUV', 12),
(47, 'Displays', 12),
(48, 'HVAC', 12),
(49, 'CPUH', 13),
(50, 'CPUV', 13),
(51, 'Displays', 13),
(52, 'HVAC', 13),
(53, 'CPUH', 14),
(54, 'CPUV', 14),
(55, 'Displays', 14),
(56, 'HVAC', 14);
