-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2012 at 08:22 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_state`
--

DROP TABLE IF EXISTS `tbl_component_state`;
CREATE TABLE IF NOT EXISTS `tbl_component_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `state` enum('green','yellow','red','blue') NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_component_state`
--

INSERT INTO `tbl_component_state` (`id`, `component_id`, `state`, `datetime`) VALUES
(1, 1, 'yellow', '2012-02-28 17:30:07'),
(2, 1, 'yellow', '2012-02-28 17:35:19'),
(3, 1, 'red', '2012-02-28 17:40:07'),
(4, 1, 'blue', '2012-02-28 17:45:19'),
(5, 1, 'green', '2012-02-28 17:50:30'),
(6, 2, 'yellow', '2012-02-28 17:55:19'),
(7, 2, 'blue', '0000-00-00 00:00:00'),
(8, 2, 'red', '0000-00-00 00:00:00'),
(9, 2, 'blue', '0000-00-00 00:00:00'),
(10, 3, 'red', '2012-02-28 17:50:30'),
(11, 3, 'yellow', '2012-02-28 17:55:19'),
(12, 3, 'yellow', '0000-00-00 00:00:00'),
(13, 4, 'red', '2012-02-28 18:05:15'),
(14, 4, 'blue', '2012-02-28 18:10:28'),
(15, 5, 'green', '2012-02-28 18:50:39'),
(16, 5, 'green', '2012-02-28 18:55:19'),
(17, 5, 'red', '0000-00-00 00:00:00'),
(18, 5, 'yellow', '2012-02-28 19:05:19'),
(19, 5, 'blue', '2012-02-28 19:10:19'),
(20, 5, 'red', '2012-02-28 19:15:19'),
(21, 5, 'red', '2012-02-28 19:20:19'),
(22, 5, 'red', '2012-02-28 19:25:19'),
(23, 5, 'yellow', '2012-02-28 19:30:19'),
(24, 5, 'blue', '2012-02-28 19:35:19'),
(25, 5, 'green', '2012-02-28 18:50:39'),
(26, 5, 'green', '2012-02-28 18:55:19'),
(27, 5, 'red', '0000-00-00 00:00:00'),
(28, 5, 'yellow', '2012-02-28 19:05:19'),
(29, 7, 'red', '2012-02-28 19:10:19'),
(30, 7, 'blue', '2012-02-28 19:15:19'),
(31, 6, 'red', '2012-02-28 19:20:19'),
(32, 6, 'yellow', '2012-02-28 19:25:19'),
(33, 8, 'red', '2012-02-28 19:30:19'),
(34, 8, 'yellow', '2012-02-28 19:35:19'),
(35, 1, 'green', '2012-02-28 17:50:32'),
(36, 1, 'yellow', '2012-02-28 17:50:36'),
(37, 1, 'red', '2012-02-28 17:50:40'),
(38, 1, 'blue', '2012-02-28 17:50:50'),
(39, 1, 'green', '2012-02-28 17:50:55'),
(40, 1, 'red', '2012-02-29 17:57:56'),
(41, 1, 'blue', '2012-02-29 17:58:10');
