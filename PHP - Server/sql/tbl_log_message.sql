-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2012 at 07:15 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_message`
--

DROP TABLE IF EXISTS `tbl_log_message`;
CREATE TABLE IF NOT EXISTS `tbl_log_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_log_message`
--

INSERT INTO `tbl_log_message` (`id`, `component_id`, `created_date`, `message`, `status`) VALUES
(2, 1, '2012-02-27 19:47:20', 'temperature 105', 3),
(3, 1, '2012-02-27 19:47:25', 'temperature in range', 1),
(4, 1, '2012-02-27 19:53:37', 'shut down', 0),
(5, 1, '2012-02-27 19:54:16', 'power on', 0),
(6, 5, '2012-02-27 19:47:20', 'temperature 105', 3),
(7, 5, '2012-02-27 19:55:00', 'temperature in range', 1),
(8, 5, '2012-02-27 19:55:17', 'shut down', 0),
(9, 5, '2012-02-27 19:55:32', 'power on', 0),
(10, 9, '2012-02-27 19:55:50', 'temperature 105', 3),
(11, 9, '2012-02-27 19:56:04', 'temperature in range', 1),
(12, 9, '2012-02-27 19:56:22', 'shut down', 0),
(13, 9, '2012-02-27 19:47:25', 'power on', 0),
(25, 2, '2012-03-01 19:19:18', 'power on', 0),
(24, 2, '2012-03-01 19:19:01', 'shut down', 0),
(23, 2, '2012-03-01 19:18:47', 'temperature in range', 1),
(22, 2, '2012-03-01 19:18:39', 'temperature 105', 3),
(26, 4, '2012-03-01 19:23:27', 'temperature 105', 3),
(27, 4, '2012-03-01 19:23:43', 'temperature in range', 1),
(28, 4, '2012-03-01 19:24:03', 'shut down', 0),
(29, 4, '2012-03-01 19:24:15', 'power on', 0);
