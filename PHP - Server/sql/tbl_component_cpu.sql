-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2012 at 06:57 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `HBMS`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_component_cpu`
--

DROP TABLE IF EXISTS `tbl_component_cpu`;
CREATE TABLE IF NOT EXISTS `tbl_component_cpu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `component_id` int(11) NOT NULL,
  `component_type` int(2) NOT NULL,
  `cpu_name` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `last_ping` time NOT NULL,
  `current_status` varchar(50) NOT NULL,
  `cpu_temp` varchar(50) NOT NULL,
  `network_speed` varchar(50) NOT NULL,
  `os_version` varchar(50) NOT NULL,
  `player_vers` varchar(50) NOT NULL,
  `last_reboot` varchar(50) NOT NULL,
  `schedule_name` varchar(50) NOT NULL,
  `last_sched_upload` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `tbl_component_cpu`
--

INSERT INTO `tbl_component_cpu` (`id`, `component_id`, `component_type`, `cpu_name`, `ip_address`, `last_ping`, `current_status`, `cpu_temp`, `network_speed`, `os_version`, `player_vers`, `last_reboot`, `schedule_name`, `last_sched_upload`) VALUES
(1, 1, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(2, 2, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(3, 5, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(4, 9, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(5, 13, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(6, 17, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(7, 21, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(8, 25, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(9, 29, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(10, 33, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(11, 37, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(12, 41, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(13, 45, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(14, 49, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(15, 53, 0, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(16, 6, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(17, 10, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(18, 14, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(19, 18, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(20, 22, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(21, 26, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(22, 30, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(23, 34, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(24, 38, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(25, 42, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(26, 46, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(27, 50, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12'),
(28, 54, 1, 'CPU007', '123.142.0.2', '10:52:00', 'online', '83oF', '.2Mbps', '10.7.1', '2.8.1', '12:04a 01/15/12', '"SCHED123"', '12:30a 01/03/12');
