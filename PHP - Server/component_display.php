<?php 
//session_start();
include("helper/DBOperation.php");
$dbObj = new DBOperation();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />


<title>HBMS - Component Display</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}
</script>


<script type="text/javascript">
	function tabblock(viewblock) {
		for(x=1; x<=5; x++) {
			if(x == viewblock) {
				document.getElementById('block'+x).style.display="block";
				document.getElementById('downarrow'+x).className="compo_dis_arrow_active";
				
				
			} else {
				document.getElementById('block'+x).style.display="none";
				document.getElementById('downarrow'+x).className="";
			}	
		}
	}
</script>

</head>

<body>


	<div id="component_cpu_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        <?php
	$device_id = $_REQUEST['did'];
	$component_id = $_REQUEST['cid'];
	$result_devicename = array();
	if($device_id) {
		$result_devicename = $dbObj->executeQuery("SELECT device_name FROM tbl_device where id=".$device_id , true);
	$row_componentcpu_topleft = $dbObj->executeQuery("SELECT * FROM tbl_component_display where component_id='".$component_id."' AND display_type = 0 ORDER BY id DESC LIMIT 1" , true);

	$row_componentcpu_topright = $dbObj->executeQuery("SELECT * FROM tbl_component_display where component_id='".$component_id."' AND display_type = 1 ORDER BY id DESC LIMIT 1" , true);

	$row_componentcpu_bottomleft = $dbObj->executeQuery("SELECT * FROM tbl_component_display where component_id='".$component_id."' AND display_type = 2 ORDER BY id DESC LIMIT 1" , true);

	$row_componentcpu_bottomright = $dbObj->executeQuery("SELECT * FROM tbl_component_display where component_id='".$component_id."' AND display_type = 3 ORDER BY id DESC LIMIT 1" , true);

	$row_componentcpu_vertical = $dbObj->executeQuery("SELECT * FROM tbl_component_display where component_id='".$component_id."' AND display_type = 4 ORDER BY id DESC LIMIT 1" , true);
	}
?>
        
        <div class="component_cpu_middle_content">
        
        	<div class="system_unit_row">
                <div class="system_top_home_bttn"><a href="system.php?id=<?php echo $device_id; ?>">Unit</a></div>
                <div class="component_cpu_txt">
                	<div class="component_cpu_txt1"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?></div>
                    <div class="component_cpu_txt2">Display</div>
                </div>
                <div class="system_unit_icon"><a href="alert_log.php?cid=<?=$component_id?>&did=<?=$device_id;?>"></a></div>
            </div>
            
            
            <div class="compo_dis_middle_menu">
            	<ul>
                	<li><a class="compo_dis_arrow_active" id="downarrow1" href="#" onclick="tabblock(1);"><div class="compo_dis_frame1"></div></a></li>
                    <li><a href="#" id="downarrow2" onclick="tabblock(2);"><div class="compo_dis_frame2"></div></a></li>
                    <li><a href="#" id="downarrow3" onclick="tabblock(3);"><div class="compo_dis_frame3"></div></a></li>
                    <li><a href="#" id="downarrow4" onclick="tabblock(4);"><div class="compo_dis_frame4"></div></a></li>
                    <li class="compo_no_divider"><a href="#" id="downarrow5" onclick="tabblock(5);"><div class="compo_dis_frame5"></div></a></li>
                </ul>
            </div>
            
            
            
    <!--div-1-start-->   
            <div class="component_cpu_middle_txt" id="block1" style="display:block;">
            	<h2>Display Info</h2>
            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display name:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['display_name'])) { 
			echo $row_componentcpu_topleft['display_name']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ip address:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['ip_address'])) { 
			echo $row_componentcpu_topleft['ip_address']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>	
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['last_ping'])) { 
			echo $row_componentcpu_topleft['last_ping']; }?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['current_status'])) { 
			echo $row_componentcpu_topleft['current_status']; } ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['display_temp'])) { 
			echo $row_componentcpu_topleft['display_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">backlight temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['backlight_temp'])) { 
			echo $row_componentcpu_topleft['backlight_temp']; }?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ctrl board temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['ctrl_board_temp'])) { 
			echo $row_componentcpu_topleft['ctrl_board_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">duty cycle:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['duty_cycle'])) { 
			echo $row_componentcpu_topleft['duty_cycle']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ambient light:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['ambient_light'])) { 
			echo $row_componentcpu_topleft['ambient_light']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">brightness:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['brightness'])) { 
			echo $row_componentcpu_topleft['brightness']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">input source:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topleft['input_source'])) { 
			echo $row_componentcpu_topleft['input_source']; }?></div>
			<div class="clear"></div>
                </div> 
             </div>
 <!--div-1-end-->       
 
 
 
 
 
 
 
 
 
 
    <!--div-2-start-->   
            <div class="component_cpu_middle_txt" id="block2" style="display:none">
            	<h2>Display Info</h2>
            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display name:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['display_name'])) { 
			echo $row_componentcpu_topright['display_name']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ip address:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['ip_address'])) { 
			echo $row_componentcpu_topright['ip_address']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>	
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['last_ping'])) { 
			echo $row_componentcpu_topright['last_ping']; }?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['current_status'])) { 
			echo $row_componentcpu_topright['current_status']; } ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['display_temp'])) { 
			echo $row_componentcpu_topright['display_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">backlight temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['backlight_temp'])) { 
			echo $row_componentcpu_topright['backlight_temp']; }?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ctrl board temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['ctrl_board_temp'])) { 
			echo $row_componentcpu_topright['ctrl_board_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">duty cycle:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['duty_cycle'])) { 
			echo $row_componentcpu_topright['duty_cycle']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ambient light:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['ambient_light'])) { 
			echo $row_componentcpu_topright['ambient_light']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">brightness:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['brightness'])) { 
			echo $row_componentcpu_topright['brightness']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">input source:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_topright['input_source'])) { 
			echo $row_componentcpu_topright['input_source']; }?></div>
			<div class="clear"></div>
                </div> 
             </div>
 <!--div-2-end-->       
 
 
 
  
    <!--div-3-start-->   
            <div class="component_cpu_middle_txt" id="block3" style="display:none">
            	<h2>Display Info</h2>
            	            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display name:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['display_name'])) { 
			echo $row_componentcpu_bottomleft['display_name']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ip address:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['ip_address'])) { 
			echo $row_componentcpu_bottomleft['ip_address']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>	
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['last_ping'])) { 
			echo $row_componentcpu_bottomleft['last_ping']; }?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['current_status'])) { 
			echo $row_componentcpu_bottomleft['current_status']; } ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['display_temp'])) { 
			echo $row_componentcpu_bottomleft['display_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">backlight temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['backlight_temp'])) { 
			echo $row_componentcpu_bottomleft['backlight_temp']; }?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ctrl board temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['ctrl_board_temp'])) { 
			echo $row_componentcpu_bottomleft['ctrl_board_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">duty cycle:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['duty_cycle'])) { 
			echo $row_componentcpu_bottomleft['duty_cycle']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ambient light:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['ambient_light'])) { 
			echo $row_componentcpu_bottomleft['ambient_light']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">brightness:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['brightness'])) { 
			echo $row_componentcpu_bottomleft['brightness']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">input source:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomleft['input_source'])) { 
			echo $row_componentcpu_bottomleft['input_source']; }?></div>
			<div class="clear"></div>
                </div> 
             </div>
 <!--div-3-end--> 
 
 
 
 
 
  
    <!--div-4-start-->   
            <div class="component_cpu_middle_txt" id="block4" style="display:none">
            	<h2>Display Info</h2>
            	  	            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display name:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['display_name'])) { 
			echo $row_componentcpu_bottomright['display_name']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ip address:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['ip_address'])) { 
			echo $row_componentcpu_bottomright['ip_address']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>	
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['last_ping'])) { 
			echo $row_componentcpu_bottomright['last_ping']; }?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['current_status'])) { 
			echo $row_componentcpu_bottomright['current_status']; } ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['display_temp'])) { 
			echo $row_componentcpu_bottomright['display_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">backlight temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['backlight_temp'])) { 
			echo $row_componentcpu_bottomright['backlight_temp']; }?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ctrl board temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['ctrl_board_temp'])) { 
			echo $row_componentcpu_bottomright['ctrl_board_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">duty cycle:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['duty_cycle'])) { 
			echo $row_componentcpu_bottomright['duty_cycle']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ambient light:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['ambient_light'])) { 
			echo $row_componentcpu_bottomright['ambient_light']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">brightness:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['brightness'])) { 
			echo $row_componentcpu_bottomright['brightness']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">input source:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_bottomright['input_source'])) { 
			echo $row_componentcpu_bottomright['input_source']; }?></div>
			<div class="clear"></div>
                </div> 
             </div>
 <!--div-4-end--> 
 
 
 
 
  
    <!--div-5-start-->   
            <div class="component_cpu_middle_txt" id="block5" style="display:none;">
            	<h2>Display Info</h2>
            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display name:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['display_name'])) { 
			echo $row_componentcpu_vertical['display_name']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ip address:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['ip_address'])) { 
			echo $row_componentcpu_vertical['ip_address']; } ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>	
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['last_ping'])) { 
			echo $row_componentcpu_vertical['last_ping']; }?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['current_status'])) { 
			echo $row_componentcpu_vertical['current_status']; } ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">display temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['display_temp'])) { 
			echo $row_componentcpu_vertical['display_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">backlight temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['backlight_temp'])) { 
			echo $row_componentcpu_vertical['backlight_temp']; }?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ctrl board temp:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['ctrl_board_temp'])) { 
			echo $row_componentcpu_vertical['ctrl_board_temp']; } ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">duty cycle:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['duty_cycle'])) { 
			echo $row_componentcpu_vertical['duty_cycle']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">ambient light:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['ambient_light'])) { 
			echo $row_componentcpu_vertical['ambient_light']; }?></div>	
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">brightness:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['brightness'])) { 
			echo $row_componentcpu_vertical['brightness']; }?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">input source:</div>
                    <div class="component_middle_txt_name_right"><?php if(isset($row_componentcpu_vertical['input_source'])) { 
			echo $row_componentcpu_vertical['input_source']; }?></div>
			<div class="clear"></div>
                </div> 
             </div>
 <!--div-5-end--> 
 
 
 
 
 
 
 
 
 
 
   
            
            
        </div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
            	<div class="unita_bottm_home"><a href="unit_a.php"></a></div>
                <div class="unita_bottm_show_email"><a href="email_log.php"></a></div>
                <div class="unita_bottm_refresh"><a href="#"></a></div>
                <div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
                <div class="unita_bottm_setting"><a href="system.php?id=1"></a></div>
            </div>
        </div>
        
        
    </div>



</body>
</html>
