<?php 

include("helper/DBOperation.php");
$dbObj = new DBOperation();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />

<title>HBMS - System</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>


</head>

<body>

<div id="system_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo "12:10 AM";//echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        <?php 
	$device_id = $_REQUEST['id'];
	$result_devicename = array();
	if($device_id) {
		$result_devicename = $dbObj->executeQuery("SELECT device_name FROM tbl_device where id=".$device_id , true);
	}

	?>
        <div class="system_unit_row">
                <div class="system_top_home_bttn"><a href="unit_a.php">Home</a></div>
                <div class="system_unit_txt"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?></div>
               <!-- <div class="system_unit_icon"><a href="alert_log.html"></a></div> -->
            </div>
        
        
        <div class="unita_middle_content_1">
        <?php
	if($device_id) {
	$componentResult = $dbObj->getRecords("tbl_components", "", array("device_id"=>$device_id), "", "");
	
	while($row_component = mysql_fetch_array($componentResult)) { 
		if($row_component['component_name']=='CPUH'){
			$link = "component_CPU.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='CPUV'){
			$link = "component_CPUV.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='Displays'){
			$link = "component_display.php?cid=".$row_component['id']."&did=".$device_id;
		}else if($row_component['component_name']=='HVAC'){
			$link = "component_HVAC.php?cid=".$row_component['id']."&did=".$device_id;
		}	
		
		
	?>
	    <div class="unita_middle_row">
			<?php		
			$qry = "select * from tbl_component_state where component_id='".$row_component['id']."' order by datetime asc limit 0,9";			
			$result = mysql_query($qry);
			$total = mysql_num_rows($result);			
			if($total<9){
				$remDiv = (9-$total);
				for($j=0;$j<=$remDiv;$j++){
					?>
		    			<div id="s" style="float:left;margin:0.5px; margin-top:16px; width:5px; height:20px; background-color:green; text-align:left;">&nbsp;</div>
				<?php
				}
				for($i=0;$i<$total;$i++){
					$row_component_color = mysql_fetch_array($result);
					$color = $row_component_color['state'];

					if($i==($total-1)){
						?>
		    				<div id="s" style="float:left;margin:0.5px; margin-top:16px; width:90px; height:20px; background-color:<?php echo $color;?>; text-align:left;">&nbsp;</div>

						<?php
					}else{
				?>
		    				<div id="s" style="float:left;margin:0.5px; margin-top:16px; width:5px; height:20px; background-color:<?php echo $color;?>; text-align:left;">&nbsp;</div>
				<?php
					}
				}

			}else{
				for($i=0;$i<=9;$i++){
					$row_component_color = mysql_fetch_array($result);

					if($row_component_color['state']=="")
					{				
						$color = "green";
					}else{						
						$color = $row_component_color['state'];
					}
					if($i==$total){
					?>
						<div id="s" style="float:left;margin:0.5px; margin-top:16px; width:90px; height:20px; background-color:<?php echo $color;?>; text-align:left;">&nbsp;</div>
					<?php	
					}else{
				?>

		    				<div id="s" style="float:left;margin:0.5px; margin-top:16px; width:5px; height:20px; background-color:<?php echo $color;?>; text-align:left;">&nbsp;</div>
				<?php }
				}
			}
			?>
                	<div class="unita_middle_txt"><?php echo $row_component['component_name']; ?></div>
                	<div class="unita_middle_arrow"><a href="<?=$link?>"></a> </div>
            	</div>
        	
	<?php } } ?>
        </div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
            	<div class="unita_bottm_home"><a href="unit_a.html"></a></div>
                <div class="unita_bottm_show_email"><a href="email_log.html"></a></div>
                <div class="unita_bottm_refresh"><a href="#" onclick="javascript:window.location.reload();"></a></div>
                <div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
                <div class="unita_bottm_setting"><a href="system.html"></a></div>
            </div>
        </div>
        
        
    </div>
</div>



</body>
</html>
