<?php 
//session_start();
include("helper/DBOperation.php");
$dbObj = new DBOperation();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">
<title>HBMS - Unit a</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<meta name="apple-mobile-web-app-capable" content="yes" />


<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>


</head>

<body>

<div id="unit_a_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        
        
        <div class="unita_middle_content">
           
	<?php 
	$compare_colors = array();
	$deviceResult = $dbObj->getRecords("tbl_device", "", "", "", "");
	while($row_devices = mysql_fetch_array($deviceResult)) { ?>
	    	<input type="hidden" name="deviceid" value="<?php echo $row_devices['id']; ?>" />
        	<div class="unita_middle_row">
			<div class="unita_progress_bar">
		
<?php
	
$color_state = array();
$color_key_msg = array();


	$componentResult = $dbObj->getRecords("tbl_components", "", array("device_id"=>$row_devices['id']), "", "");

	$total_components = mysql_num_rows($componentResult);

	$color_key_msg = array();
	$color_key_msg['green'] = "online";
	$color_key_msg['yellow'] = "issue";
	$color_key_msg['red'] = "attention";
	$color_key_msg['blue'] = "NA";
	
	$color_priority = array();
	$color_priority['green'] = 1;
	$color_priority['yellow'] = 2;
	$color_priority['red'] = 3;
	$color_priority['blue'] = 4;

	$color_code = array();
	$color_code[1] = "green";
	$color_code[2] = "yellow";
	$color_code[3] = "red";
	$color_code[4] = "blue";
	$y=0;
	$compare_colors = array();
	while($row_component = mysql_fetch_array($componentResult)) {

		$qry = "select * from tbl_component_state where component_id='".$row_component['id']."' order by datetime DESC limit 27";
		$result = mysql_query($qry);
		$total = mysql_num_rows($result);	
		$x = 0;
		$color_state = array();
		while($row=mysql_fetch_array($result))
		{
			if($x%3 == 0) {
				$color_state[] = $row['state'];
			}
			$x++;
		}
		for($z=0; $z<9; $z++) { 
		
			if(isset($color_state[$z])) {
				$strip_color = $color_state[$z];	
			} else {
				$strip_color = "blue";
			}
			$compare_colors[$y][$z] = $strip_color;
		}
		$y++;

	}
//print_r($compare_colors);
//echo "<br/>";

		for($t=0; $t<9; $t++) { 
			$result_color = 1;
			for($p=0; $p<$total_components; $p++) {
				if($result_color < $color_priority[$compare_colors[$p][$t]]){
					$result_color = $color_priority[$compare_colors[$p][$t]];
				}
			}
			
			if($result_color) {
				$strip_color = $color_code[$result_color];	
			} else {
				$strip_color = "green";
			}
			if($t==0) {
				$className = "big_strips";
				$msg = $color_key_msg[$strip_color];
			} else {
				$className = "small_strips";
				$msg = "";
			}
			?>
			
			<div id="s" class="<?php echo $className; ?>" style="background-color:<?php echo $strip_color; ?>" ><?php echo $msg; ?></div>
			
		<?php } ?>
			</div>
			<div class="unita_middle_txt" style="width:auto;"><?php echo $row_devices['device_name']; ?></div>
			<div class="unita_middle_arrow"><a href="system.php?id=<?php echo $row_devices['id'];?>"></a></div>
			
            	</div>
	<?php } ?>
 
        </div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
            	<div class="unita_bottm_home"><a class="unita_bottm_active_home" href="unit_a.php"></a></div>
                <div class="unita_bottm_show_email"><a href="email_log.php"></a></div>
                <div class="unita_bottm_refresh"><a href="#"></a></div>
                <div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
                <div class="unita_bottm_setting"><a href="system.php?id=1"></a></div>
		
            </div>
        </div>
        
        
    </div>
</div>



</body>
</html>
