<?php 
//session_start();
include("helper/DBOperation.php");
$dbObj = new DBOperation();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name = "viewport" content = "width = device-width">

<meta name="apple-mobile-web-app-capable" content="yes" />

<title>HBMS - Component CPU</title>
<link href="HBMS_css.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
// Mobile Safari in standalone mode
if(("standalone" in window.navigator) && window.navigator.standalone){

// If you want to prevent remote links in standalone web apps opening Mobile Safari, change 'remotes' to true
var noddy, remotes = false;

document.addEventListener('click', function(event) {

noddy = event.target;

// Bubble up until we hit link or top HTML element. Warning: BODY element is not compulsory so better to stop on HTML
while(noddy.nodeName !== "A" && noddy.nodeName !== "HTML") {
noddy = noddy.parentNode;
}

if('href' in noddy && noddy.href.indexOf('http') !== -1 && (noddy.href.indexOf(document.location.host) !== -1 || remotes)) {
event.preventDefault();
document.location.href = noddy.href;
}

},false);
}

</script>


</head>

<body>

<div id="component_cpu_main">
	<div class="unit_a_content">
    
    	<div class="unit_a_top_row">
        	<div class="unit_a_cemusa_img"></div>
            <div class="unita_top_text_part">
            	<div class="unita_top_txt1"><span>logged in as:</span> <?=$_SESSION['userslog']['display_name']?></div>
                <div class="unita_top_txt1"><span>last update:</span> 
		<?php echo $_SESSION['userslog']['last_login_date'];?></div>
            </div>
        </div>
        <?php
	$device_id = $_REQUEST['did'];
	$component_id = $_REQUEST['cid'];
	$result_devicename = array();
	if($device_id) {
		$result_devicename = $dbObj->executeQuery("SELECT device_name FROM tbl_device where id=".$device_id , true);
	$row_componentcpu = $dbObj->executeQuery("SELECT * FROM tbl_component_cpu where component_id='".$component_id."' ORDER BY id DESC LIMIT 1" , true);
	}
?>
        
        <div class="component_cpu_middle_content">
        
        	<div class="system_unit_row">
                <div class="system_top_home_bttn"><a href="system.php?id=<?php echo $device_id; ?>">Unit</a></div>
                <div class="component_cpu_txt">
                	<div class="component_cpu_txt1"><?php if(isset($result_devicename['device_name'])) { 
			echo $result_devicename['device_name']; } ?></div>
                    <div class="component_cpu_txt2">CPUH</div>
                </div>
                <div class="system_unit_icon"><a href="alert_log_cpuh.php?cid=<?=$component_id;?>&did=<?=$device_id;?>"></a></div>
            </div>
            
            <div class="component_cpu_middle_txt">
            	<h2>CPU Info</h2>
            	<div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">cpu name:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['cpu_name']; ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                  <div class="component_middle_txt_name_left">ip address:</div>
                   <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['ip_address']; ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last ping:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['last_ping']; ?></div>
			<div class="clear"></div>
                </div>   
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">current status:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['current_status']; ?></div>
			<div class="clear"></div>
                </div>
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">cpu temp:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['cpu_temp']; ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">network speed:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['network_speed']; ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">os version:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['os_version']; ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">player vers:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['player_vers']; ?></div>
			<div class="clear"></div>
                </div>  
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last reboot:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['last_reboot']; ?></div>
			<div class="clear"></div>
		</div>
 		<div class="component_middle_txt_row">

                	<div class="component_middle_txt_name_left">free HD space:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['free_hd_space']; ?></div>
			<div class="clear"></div>
                </div>
                
                
                <h2 class="space">Content Info</h2>
                
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">schedule name:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['schedule_name']; ?></div>
			<div class="clear"></div>
                </div> 
                <div class="component_middle_txt_row">
                	<div class="component_middle_txt_name_left">last sched upload:</div>
                    <div class="component_middle_txt_name_right"><?php echo $row_componentcpu['last_sched_upload']; ?></div>
			<div class="clear"></div>
                </div> 


                
                 
             </div>
        
   
            
            
        </div>
        
        <div class="unita_showtell_row">
        	<div class="unita_showtell_img"></div>
        </div>
        
        <div class="unita_bottm_menu">
        	<div class="unita_bottm_menu_row">
            	<div class="unita_bottm_home"><a href="unit_a.php"></a></div>
                <div class="unita_bottm_show_email"><a href="email_log.php"></a></div>
                <div class="unita_bottm_refresh"><a href="#" onclick="javascript:window.location.reload();"></a></div>
                <div class="unita_bottm_send_note"><a href="mailto:reachshowtell@showtell.com"></a></div>
                <div class="unita_bottm_setting"><a href="system.php?id=1"></a></div>
            </div>
        </div>
        
        
    </div>
</div>



</body>
</html>
